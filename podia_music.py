#!/usr/bin/env python3

import colors
import json
import numpy as np
import pyaudio
import socket
import struct
import sys
from collections import deque

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
RECORD_SECONDS = 5
NB_LEDS = 8  # Must be a power of 2
SEND = True
MAX_VAL = 25

if SEND:
    if len(sys.argv) < 2:
        print("Usage: "+sys.argv[0]+" SERVER_ADDRESS")
        sys.exit()
    (HOST, PORT) = (sys.argv[1], 4242)
else:
    (HOST, PORT) = ("", 0)
VERBOSE = True
MAX_LEVEL = 25
MAXLEN = 5

older_values = deque(maxlen=MAXLEN)


def calculate_levels(frames, chunk, sample_rate, nb_rows, max_val=MAX_VAL):
    """Computes levels through a FFT"""
    # Convert raw data to numpy array
    data = np.frombuffer(frames, np.int16)

    # Apply FFT - real data so rfft used
    fourier = np.fft.rfft(data)
    # Remove last element in array to make it the same size as chunk
    fourier = np.delete(fourier, len(fourier) - 1)
    # Find amplitude
    power = np.log10(np.abs(fourier))**2

    # Arrange array into nb_rows rows for the nb_rows LEDs
    power = np.reshape(power, (nb_rows, chunk / 2 / nb_rows))
    matrix = np.float_(np.average(power, axis=1)) / max_val
    return matrix

def get_means(values):
    somme = [0 for j in range(len(older_values[0]))]
    for i in range(len(older_values)):
        for j in range(len(older_values[i])):
            somme[j] += older_values[i][j]
    return [j / MAXLEN for j in somme]

def build_instructions(matrix_levels):
    """Build instructions to send to the LEDs, mapping amplitudes to colors"""
    older_values.append(matrix_levels)
    matrix_levels = get_means(matrix_levels)
    colorset = {k: colors.hsv_to_rgb255({'h': int(matrix_levels[k] * 360),
                                         's': 1,
                                         'v': 1})
                for k in range(len(matrix_levels))}
    return {"fading": False, "colors": colorset}

def send_instructions(host, port, data):
    """Sends instructions to the LEDs server to display the current
    configuration

    params:
        data is a dict {fading: bool, colors: {}}
    """
    if len(data["colors"]) == 0:
        return

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        sock.connect((host, port))
        data = json.dumps(data) + "\n"
        sock.sendall(data.encode('utf-8'))
        received = sock.recv(1024).decode('utf-8')
        try:
            if int(received) != len(data.strip()):
                print("Error while sending instructions for LEDs")
        except:
            print("Error while sending instructions for LEDs")
    finally:
        sock.close()

try:
    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)

    print("* recording")

    while True:
        frames = stream.read(CHUNK)
        if frames == b'\x00'.join([b'' for i in range(CHUNK * 2 + 1)]):
            levels = np.array([0 for i in range(NB_LEDS)])
            instructions = {"fading": False, "colors": {k: {'r': 0, 'g': 0, 'b':
                                                            0} for k in
                                                        range(NB_LEDS)}}
        else:
            levels = calculate_levels(frames, CHUNK, RATE, NB_LEDS)
            instructions = build_instructions(levels)
        if VERBOSE:
            print(levels)
        if SEND:
            send_instructions(HOST, PORT, instructions)

except KeyboardInterrupt:
    pass
finally:
    stream.stop_stream()
    stream.close()
    p.terminate()
